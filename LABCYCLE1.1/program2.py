#integer value
integerValue=3
print("Integer value is",integerValue)
print("Type of integer value is",type(integerValue))

# Floating point value
floatValue=3.14
print("Float value is",floatValue)
print("Type of float value is",type(floatValue))


# Complex value
complexValue=3+4j
print("Complex value is",complexValue)
print("Type of complex value is",type(complexValue))
