# Program to Get a string from an input string where all occurrences of the first 
# character are replaced with ‘$’, except the first character. [eg: onion -> oni$n]

# Function to replace the occurence of a character with $
def change_char(str1):
    char = str1[0]
    str1 = str1.replace(char,'$') # replace the character with $
    str1 = char + str1[1:]
    return str1
str = input("Enter a string: ")
print(change_char(str))
